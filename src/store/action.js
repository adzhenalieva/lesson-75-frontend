import axios from '../axios-api';

export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_SUCCESS_ENCODE = 'DATA_SUCCESS_ENCODE';
export const DATA_SUCCESS_DECODE = 'DATA_SUCCESS_DECODE';
export const DATA_FAILURE = 'DATA_FAILURE';
export const VALUE_CHANGE = 'VALUE_CHANGE';


export const dataRequest = () => ({type: DATA_REQUEST});
export const dataSuccessEncode = response => ({type: DATA_SUCCESS_ENCODE, response});
export const dataSuccessDecode = response => ({type: DATA_SUCCESS_DECODE, response});
export const dataFailure = error => ({type: DATA_FAILURE, error});


export const valueChange = (name, value) => ({type: VALUE_CHANGE, name, value});

export const fetchDataEncode = data => {
        return dispatch => {
            dispatch(dataRequest());
            axios.post('/encode', data).then(response => {
                dispatch(dataSuccessEncode(response.data.encoded));
            }, error => {
                dispatch(dataFailure(error));
            });
        }
};

export const fetchDataDecode = data => {
    return dispatch => {
        dispatch(dataRequest());
        axios.post('/decode', data).then(response => {
            dispatch(dataSuccessDecode(response.data.decoded));
        }, error => {
            dispatch(dataFailure(error));
        });
    }
};