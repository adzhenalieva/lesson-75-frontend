import {DATA_FAILURE, DATA_REQUEST, DATA_SUCCESS_DECODE, DATA_SUCCESS_ENCODE, VALUE_CHANGE} from "./action";

const initialState = {
    password: '',
    loading: false,
    error: null,
    decode: '',
    encode: ''

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_SUCCESS_ENCODE:
            return {
                ...state,
                encode: action.response,
                decode: '',
                password: '',
                loading: false
            };
        case DATA_SUCCESS_DECODE:
            return {
                ...state,
                decode: action.response,
                encode: '',
                password: '',
                loading: false
            };
        case DATA_REQUEST:
            return {
                ...state, loading: true
            };
        case DATA_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case VALUE_CHANGE:
            return {
                ...state,
                [action.name]: action.value
            };
        default:
            return state;
    }
};
export default reducer;