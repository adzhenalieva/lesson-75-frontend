import React, {Component} from 'react';
import './App.css';
import {connect} from "react-redux";
import {fetchDataDecode, fetchDataEncode, valueChange} from "./store/action";
import Spinner from "./UI/Spinner/Spinner";


const password = "password";

class App extends Component {

    sendDataEncode = data => {
        if (this.props.password !== password) {
            alert('Enter correct password');
        } else if (this.props.password === password) {
            let encode = {
                password: this.props.password,
                message: data
            };
            this.props.sendDataEncode(encode)
        }

    };

    sendDataDecode = data => {
        if (this.props.password !== 'password') {
            alert('Enter correct password');
        } else if (this.props.password === 'password') {
            let decode = {
                password: this.props.password,
                message: data
            };
            this.props.sendDataDecode(decode)
        }

    };

    inputChangeHandler = event => {
        this.props.valueChange(event.target.name, event.target.value);
    };

    render() {
        return (
            this.props.loading ?
                <Spinner/> :
            <div className="App">
                <p>Decoded message</p>
                <textarea name="decode" cols="30" rows="10"
                          value={this.props.decode}
                          onChange={this.inputChangeHandler}/>
                <p>Password</p>
                <input type="text" name="password"
                       value={this.props.password}
                       onChange={this.inputChangeHandler}/>
                <button onClick={() => this.sendDataDecode(this.props.encode)}>&#9650;</button>
                <button onClick={() => this.sendDataEncode(this.props.decode)}>&#9660;</button>
                <p>Encoded message</p>
                <textarea name="encode" cols="30" rows="10"
                          value={this.props.encode}
                          onChange={this.inputChangeHandler}/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        encode: state.encode,
        decode: state.decode,
        password: state.password,
        loading: state.loading
    }

};

const mapDispatchToProps = dispatch => ({
    valueChange: (name, value) => dispatch(valueChange(name, value)),
    sendDataEncode: data => dispatch(fetchDataEncode(data)),
    sendDataDecode: data => dispatch(fetchDataDecode(data))

});

export default connect(mapStateToProps, mapDispatchToProps)(App);
